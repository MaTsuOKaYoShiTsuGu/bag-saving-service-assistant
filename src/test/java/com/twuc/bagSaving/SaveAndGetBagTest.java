package com.twuc.bagSaving;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.List;

import static com.twuc.bagSaving.CabinetFactory.createCabinetWithPlentyOfCapacity;
import static org.junit.jupiter.api.Assertions.*;

class SaveAndGetBagTest extends BagSavingArgument {
    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createCabinetWithOnlyOneEmptyLockerAndSavableSizes")
    void should_get_a_ticket_when_saving_a_bag(Cabinet cabinet, BagSize bagSize, LockerSize lockerSize) {
        Ticket ticket = cabinet.save(new Bag(bagSize), lockerSize);
        assertNotNull(ticket);
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createCabinetWithOnlyOneEmptyLockerAndSavableSizes")
    void should_save_and_get_bag_when_locker_is_empty(Cabinet cabinet, BagSize bagSize, LockerSize lockerSize) {
        Bag savedBag = new Bag(bagSize);
        Ticket ticket = cabinet.save(savedBag, lockerSize);
        Bag fetchedBag = cabinet.getBag(ticket);
        assertSame(savedBag, fetchedBag);
    }

    @ParameterizedTest
    @MethodSource({"com.twuc.bagSaving.BagSavingArgument#createSavableBagSizeAndLockerSize"})
    void should_save_smaller_bag_to_bigger_locker(
        BagSize smallerBagSize, LockerSize biggerLockerSize) {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();

        Bag smallerBag = new Bag(smallerBagSize);
        Ticket ticket = cabinet.save(smallerBag, biggerLockerSize);
        Bag receivedBag = cabinet.getBag(ticket);

        assertSame(smallerBag, receivedBag);
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createNonSavableBagSizeAndLockerSize")
    void should_throw_when_saving_bigger_bag_to_smaller_locker(BagSize biggerBagSize, LockerSize smallerLockerSize) {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        IllegalArgumentException exception =
            assertThrows(IllegalArgumentException.class,
                () -> cabinet.save(new Bag(biggerBagSize), smallerLockerSize));

        assertEquals(
            String.format("Cannot save %s bag to %s locker.", biggerBagSize, smallerLockerSize),
            exception.getMessage());
    }

    @Test
    void should_throw_if_locker_size_is_not_specified() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        assertThrows(
            IllegalArgumentException.class,
            () -> cabinet.save(new Bag(BagSize.BIG), null));
    }

    @Test
    void should_throw_if_no_ticket_is_provided() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();

        final IllegalArgumentException error = assertThrows(
            IllegalArgumentException.class,
            () -> cabinet.getBag(null));
        assertEquals("Please use your ticket.", error.getMessage());
    }

    @Test
    void should_throw_if_ticket_is_not_provided_by_cabinet() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        Ticket ticket = new Ticket();

        final IllegalArgumentException exception = assertThrows(
            IllegalArgumentException.class,
            () -> cabinet.getBag(ticket));
        assertEquals("Invalid ticket.", exception.getMessage());
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createSavableBagSizeAndLockerSize")
    void should_throw_if_ticket_is_used(BagSize bagSize, LockerSize lockerSize) {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        Bag bag = new Bag(bagSize);
        Ticket ticket = cabinet.save(bag, lockerSize);
        cabinet.getBag(ticket);

        final IllegalArgumentException exception = assertThrows(
            IllegalArgumentException.class,
            () -> cabinet.getBag(ticket));
        assertEquals("Invalid ticket.", exception.getMessage());
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createSavableBagSizeAndLockerSize")
    void should_throw_if_ticket_is_generated_by_another_cabinet(BagSize bagSize, LockerSize lockerSize) {
        Cabinet anotherCabinet = createCabinetWithPlentyOfCapacity();
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();

        Ticket generatedByAnotherCabinet = anotherCabinet.save(new Bag(bagSize), lockerSize);

        final IllegalArgumentException exception = assertThrows(
            IllegalArgumentException.class,
            () -> cabinet.getBag(generatedByAnotherCabinet));
        assertEquals("Invalid ticket.", exception.getMessage());
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createCabinetWithOnlyOneLockerSizeFull")
    void should_throw_if_correspond_lockers_are_full(
        Cabinet fullCabinet, BagSize bagSize, LockerSize lockerSize) {
        Bag savedBag = new Bag(bagSize);
        InsufficientLockersException error = assertThrows(
            InsufficientLockersException.class,
            () -> fullCabinet.save(savedBag, lockerSize));

        assertEquals("Insufficient empty lockers.", error.getMessage());
    }

    @ParameterizedTest
    @EnumSource(value = LockerSize.class, mode = EnumSource.Mode.EXCLUDE)
    void should_throw_when_saving_nothing(LockerSize lockerSize) {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        final IllegalArgumentException exception = assertThrows(
            IllegalArgumentException.class,
            () -> cabinet.save(null, lockerSize));

        assertEquals("Please at least put something here.", exception.getMessage());
    }

    @Test
    void should_save_bag_success_with_assistant_when_save_bag_using_assistant() {
        SaveAssistant assistant = new SaveAssistant();
        Bag bag = new Bag(BagSize.MEDIUM);
        Cabinet cabinet = new Cabinet(LockerSetting.of(LockerSize.MEDIUM,10));
        Ticket ticket = assistant.saveBagToCabinet(bag,cabinet);
        assertNotNull(ticket);
    }

    @Test
    void given_a_cabinet_without_medium_size_and_a_cabinet_with_enough_medium_size_when_save_medium_bag_with_assistant_then_should_return_success() {
        SaveAssistant assistant = new SaveAssistant();
        Bag bag = new Bag(BagSize.MEDIUM);
        Cabinet cabinetWithoutMediumSize = new Cabinet(LockerSetting.of(LockerSize.BIG,10));
        Cabinet cabinetWithEnoughMediumSize = new Cabinet(LockerSetting.of(LockerSize.MEDIUM,10));
        List<Cabinet> cabinets = new ArrayList<>();
        cabinets.add(cabinetWithoutMediumSize);
        cabinets.add(cabinetWithEnoughMediumSize);
        Ticket ticket = assistant.saveBagToCabinet(bag,cabinets);
        assertNotNull(ticket);
    }

    @Test
    void given_a_cabinet_with_big_size_when_save_medium_bag_with_intelligent_assistant_then_should_return_success() {
        SaveAssistant assistant = new SaveAssistant();
        Bag bag = new Bag(BagSize.MEDIUM);
        Cabinet cabinetWithoutMediumSize = new Cabinet(LockerSetting.of(LockerSize.BIG,10));
        assistant.getCabinets().add(cabinetWithoutMediumSize);
        Ticket ticket = assistant.saveBagToCabinetWithIntelligence(bag);
        assertNotNull(ticket);
    }

    @Test
    void given_a_cabinet_without_size_and_a_cabinet_with_enough_big_size_when_save_medium_bag_with_assistant_then_should_return_success() {
        SaveAssistant assistant = new SaveAssistant();
        Bag bag = new Bag(BagSize.MEDIUM);
        Cabinet cabinetWithoutSize = new Cabinet(LockerSetting.of(LockerSize.BIG,1));
        cabinetWithoutSize.save(new Bag(BagSize.BIG),LockerSize.BIG);
        Cabinet cabinetWithEnoughBigSize = new Cabinet(LockerSetting.of(LockerSize.BIG,10));
        assistant.getCabinets().add(cabinetWithoutSize);
        assistant.getCabinets().add(cabinetWithEnoughBigSize);
        Ticket ticket = assistant.saveBagToCabinetWithIntelligence(bag);
        assertNotNull(ticket);
    }

    @Test
    void given_two_assistant_manage_same_cabinet_when_save_bag_with_one_of_them_then_the_other_could_get_bag() {
        SaveAssistant assistant = new SaveAssistant();
        SaveAssistant otherAssistant = new SaveAssistant();
        Cabinet cabinet = new Cabinet(LockerSetting.of(LockerSize.MEDIUM,10));
        assistant.getCabinets().add(cabinet);
        otherAssistant.getCabinets().add(cabinet);
        Ticket ticket = assistant.saveBagToCabinetWithIntelligence(new Bag(BagSize.MEDIUM));
        Bag bag = otherAssistant.getBagFromCabinets(ticket);
        assertNotNull(bag);
    }

    @Test
    void given_a_cabinet_without_size_and_a_cabinet_with_enough_big_size_when_save_medium_bag_with_skilled_assistant_then_should_save_bag_into_big_size() {
        SaveAssistant assistant = new SaveAssistant();
        Bag bag = new Bag(BagSize.MEDIUM);
        Cabinet cabinetWithoutSize = new Cabinet(LockerSetting.of(LockerSize.BIG,1));
        cabinetWithoutSize.save(new Bag(BagSize.BIG),LockerSize.BIG);
        Cabinet cabinetWithEnoughBigSize = new Cabinet(LockerSetting.of(LockerSize.BIG,10));
        assistant.getCabinets().add(cabinetWithoutSize);
        assistant.getCabinets().add(cabinetWithEnoughBigSize);
        Ticket ticket = assistant.saveBagToCabinetWithIntelligence(bag);
        assertNotNull(ticket);
        assertNotNull(cabinetWithEnoughBigSize.getBag(ticket));
    }

    @Test
    void given_a_cabinet_without_medium_size_but_with_enough_big_size_and_a_cabinet_with_enough_medium_size_when_save_medium_bag_with_lazy_assistant_then_should_save_bag_into_big_size() {
        SaveAssistant assistant = new SaveAssistant();
        Bag bag = new Bag(BagSize.MEDIUM);
        Cabinet cabinetWithoutMediumSize = new Cabinet(LockerSetting.of(LockerSize.MEDIUM,1),LockerSetting.of(LockerSize.BIG,10));
        cabinetWithoutMediumSize.save(new Bag(BagSize.MEDIUM),LockerSize.MEDIUM);
        Cabinet cabinetWithEnoughMediumSize = new Cabinet(LockerSetting.of(LockerSize.MEDIUM,10));
        assistant.getCabinets().add(cabinetWithoutMediumSize);
        assistant.getCabinets().add(cabinetWithEnoughMediumSize);
        Ticket ticket = assistant.saveBagToCabinetWithLazy(bag);
        assertNotNull(ticket);
        assertNotNull(cabinetWithoutMediumSize.getBag(ticket));
    }

    @Test
    void given_a_manager_when_save_bag_with_manager_then_should_save_and_get_by_manager() {
        Manager manager = new Manager();
        SaveAssistant assistant = new SaveAssistant();
        Cabinet cabinet = new Cabinet(LockerSetting.of(LockerSize.MEDIUM,10));
        Bag bag = new Bag(BagSize.MEDIUM);
        assistant.getCabinets().add(cabinet);
        manager.getAssistants().add(assistant);
        Ticket ticket = manager.saveBag(bag);
        assertNotNull(ticket);
        assertSame(bag,manager.getBag(ticket));
    }
}
