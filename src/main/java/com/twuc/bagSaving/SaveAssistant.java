package com.twuc.bagSaving;

import java.util.ArrayList;
import java.util.List;

public class SaveAssistant {
    private List<Cabinet>cabinets = new ArrayList<>();

    public List<Cabinet> getCabinets() {
        return cabinets;
    }

    private LockerSize getLockerSize(BagSize bagSize) {
        for (LockerSize lockerSize : LockerSize.values()) {
            if (lockerSize.getSizeNumber() == bagSize.getSizeNumber()) {
                return lockerSize;
            }
        }
        throw new IllegalArgumentException("Not supported locker size: " + bagSize);
    }

    //
    public Ticket saveBagToCabinet(Bag bag, Cabinet cabinet) {
        return cabinet.save(bag, getLockerSize(bag.getBagSize()));
    }

    //
    public Ticket saveBagToCabinet(Bag bag, List<Cabinet> cabinets) {
        for (Cabinet cabinet : cabinets) {
            try {
                return cabinet.save(bag, getLockerSize(bag.getBagSize()));
            } catch (Exception ignored) {
            }
        }
        throw new IllegalArgumentException("No supported locker could save this bag");
    }

    private LockerSize getLargerLockerSize(LockerSize currentLockerSize) {
        for (LockerSize lockerSize : LockerSize.values()) {
            if (lockerSize.getSizeNumber() > currentLockerSize.getSizeNumber()) {
                currentLockerSize = lockerSize;
                break;
            }
        }
        return currentLockerSize;
    }

    public Ticket saveBagToCabinetWithIntelligence(Bag bag) {
        LockerSize currentFindSize = getLockerSize(bag.getBagSize());
        while (currentFindSize.getSizeNumber() <= LockerSize.BIG.getSizeNumber()) {
            for (Cabinet cabinet : cabinets) {
                try {
                    return cabinet.save(bag, currentFindSize);
                } catch (Exception ignored) {
                }
            }
            if (currentFindSize == LockerSize.BIG) {
                break;
            }
            currentFindSize = getLargerLockerSize(currentFindSize);
        }
        throw new IllegalArgumentException("No supported locker could save this bag");
    }

    public Bag getBagFromCabinets(Ticket ticket) {
        for (Cabinet cabinet : cabinets) {
            try {
                return cabinet.getBag(ticket);
            } catch (Exception ignored) {
            }
        }
        throw new IllegalArgumentException("The Bag is not in these cabinets");
    }

    public Ticket saveBagToCabinetWithLazy(Bag bag) {
        for (Cabinet cabinet : cabinets) {
            LockerSize currentFindSize = getLockerSize(bag.getBagSize());
            while (currentFindSize.getSizeNumber() <= LockerSize.BIG.getSizeNumber()) {
                try {
                    return cabinet.save(bag, currentFindSize);
                } catch (Exception ignored) {
                }
                if (currentFindSize == LockerSize.BIG) {
                    break;
                }
                currentFindSize = getLargerLockerSize(currentFindSize);
            }
        }
        throw new IllegalArgumentException("No supported locker could save this bag");
    }
}
