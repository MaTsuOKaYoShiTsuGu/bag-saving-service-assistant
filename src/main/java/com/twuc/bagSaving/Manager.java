package com.twuc.bagSaving;

import java.util.ArrayList;
import java.util.List;

public class Manager {
    private List<SaveAssistant> assistants = new ArrayList<>();

    public List<SaveAssistant> getAssistants() {
        return assistants;
    }

    public Ticket saveBag(Bag bag){
        for (SaveAssistant assistant : assistants){
            try {
                return assistant.saveBagToCabinetWithIntelligence(bag);
            } catch (Exception ignored) {
            }
        }
        throw new IllegalArgumentException("");
    }

    public Bag getBag(Ticket ticket) {
        for (SaveAssistant assistant : assistants) {
            try {
                return assistant.getBagFromCabinets(ticket);
            } catch (Exception ignored) {

            }
        }
        throw new IllegalArgumentException("");
    }
}
